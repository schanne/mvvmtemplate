package com.bornfight.appname.testDemo.repoTest

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bornfight.appname.testDemo.data.TestData
import com.bornfight.common.mvvm.basemodels.BaseError
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.Status
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import com.bornfight.demo.weather.repository.weather.WeatherRepo
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class WeatherRepoTest {

    @Mock
    lateinit var weatherRepo: WeatherRepo

    val testData: TestData = TestData()
    val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getWeatherResponse_Success() {
        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getWeather(), null))
        )

        val testObserver = TestObserver<ResourceState<WeatherResponse>>()
        weatherRepo.getCurrentWeatherForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().data

        assertEquals(testData.getWeather(), result)
    }

    @Test
    fun getWeatherResponse_Error() {
        val error = BaseError("Error went down")

        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.ERROR, null, error))
        )

        val testObserver = TestObserver<ResourceState<WeatherResponse>>()
        weatherRepo.getCurrentWeatherForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().error

        assertEquals(error, result)
    }

    @Test
    fun getForecastResponse_Success() {
        Mockito.`when`(weatherRepo.getForecastForCity("Zagreb")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getForecastData(), null))
        )

        val testObserver = TestObserver<ResourceState<List<Forecast>>>()
        weatherRepo.getForecastForCity("Zagreb").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().data

        assertEquals(testData.getForecastData(), result)
    }

    @Test
    fun getForecastResponse_Error() {
        val error = BaseError("Error went down")

        Mockito.`when`(weatherRepo.getForecastForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.ERROR, null, error))
        )

        val testObserver = TestObserver<ResourceState<List<Forecast>>>()
        weatherRepo.getForecastForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().error

        assertEquals(error, result)
    }

}