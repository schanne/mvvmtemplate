package com.bornfight.appname.testDemo.repoTest

import android.os.Build
import com.bornfight.appname.testDemo.data.TestData
import com.bornfight.common.mvvm.basemodels.BaseError
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.Status
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionRepo
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])
class SearchSuggestionRepoTest {

    private val testScheduler: TestScheduler = TestScheduler()
    private val testData: TestData = TestData()

    @Mock
    lateinit var searchSuggestionRepo: SearchSuggestionRepo

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getSearchSuggestions_Success() {

        Mockito.`when`(searchSuggestionRepo.getSearchSuggestions()).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getSearchSugeestions(), null))
        )

        val testObserver: TestObserver<ResourceState<List<SearchSuggestion>>> = TestObserver()
        searchSuggestionRepo.getSearchSuggestions().subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val resultFirst = testObserver.values().first()
        val resultData = resultFirst.data

        assertEquals(resultData, testData.getSearchSugeestions())

    }

    @Test
    fun getSearchSuggestions_Error() {

        val baseError: BaseError = BaseError("Something went wrong")
        val resourceState: ResourceState<List<SearchSuggestion>> =
            ResourceState(Status.ERROR, null, baseError)

        Mockito.`when`(searchSuggestionRepo.getSearchSuggestions())
            .thenReturn(Observable.just(resourceState))

        val testObserver: TestObserver<ResourceState<List<SearchSuggestion>>> = TestObserver()
        searchSuggestionRepo.getSearchSuggestions().subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val resultFirst = testObserver.values().first()
        val resultError = resultFirst.error

        assertEquals(resultError, baseError)
    }

}