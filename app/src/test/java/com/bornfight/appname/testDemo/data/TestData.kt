package com.bornfight.appname.testDemo.data

import com.bornfight.demo.weather.model.*

class TestData {

    fun getSearchSugeestions() = mutableListOf(
        SearchSuggestion(1, "Zagreb"),
        SearchSuggestion(2, "Osijek"),
        SearchSuggestion(3, "Kutina"),
        SearchSuggestion(4, "Nova Gradiska")
    )

    fun getSavableSearchSuggestion() = SearchSuggestion(5, "Pozega")

    fun getForecastData() = mutableListOf(
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-15 12:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-15 15:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-15 18:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-15 21:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-16 00:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-16 03:00:00", "Zagreb")
    )

    fun getSortedForecastData() = mutableListOf(
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-16 00:00:00", "Zagreb"),
        Forecast(Main(1.0, 1.0, 1.0), listOf(), "2019-10-16 03:00:00", "Zagreb")
    )

    fun getWeather() = WeatherResponse(
        2643743,
        listOf(
            Weather(803, "Clouds", "broken clouds", "04d")
        ),
        Main(280.5, 279.15, 282.04),
        "London",
        1573228450
    )

    fun getForecastJSonTest() = ForecastResponse(
        mutableListOf(
            Forecast(
                Main(286.71, 285.55, 286.71),
                listOf(Weather(804, "Clouds", "overcast clouds", "04d")),
                "2019-11-11 15:00:00"
            ),
            Forecast(
                Main(290.06, 289.19, 290.06),
                listOf(Weather(803, "Clouds", "broken clouds", "04d")),
                "2019-11-11 18:00:00"
            ),
            Forecast(
                Main(288.73, 288.15, 288.73),
                listOf(Weather(804, "Clouds", "overcast clouds", "04d")),
                "2019-11-11 21:00:00"
            ),
            Forecast(
                Main(284.79, 284.5, 284.79),
                listOf(Weather(500, "Rain", "light rain", "10n")),
                "2019-11-12 00:00:00"
            )
        )
    )

}