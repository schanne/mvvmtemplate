package com.bornfight.appname.test.viewModelTest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bornfight.appname.test.data.TestData
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.common.scheduler.TestSchedulerProvider
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class WeatherViewModelTest {

    //https://medium.com/mindorks/unit-testing-for-viewmodel-19f4d76b20d4

    /**
     * @see instantTaskExecutorRule: https://developer.android.com/reference/androidx/arch/core/executor/testing/InstantTaskExecutorRule.html?authuser=1
     * @param schedulerProvider: is an implementation of TestScheduler, all schedulers use trampoline()
     * */

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    var schedulerProvider: SchedulerProvider = TestSchedulerProvider()

    /*
    @Mock
    lateinit var weatherRepo: WeatherRepo

    @Mock
    lateinit var currentWeatherObserver: Observer<ResourceState<WeatherResponse>>

    @Mock
    lateinit var forecastListObserver: Observer<ResourceState<List<Forecast>>>

    lateinit var viewModel: WeatherViewModel
     */

    private val testData = TestData()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        /*
        viewModel = WeatherViewModel(schedulerProvider, weatherRepo)
        viewModel.weatherData.observeForever(currentWeatherObserver)
        viewModel.forecastData.observeForever(forecastListObserver)
         */
    }

    @After
    fun cleanUp() {
        // viewModel.weatherData.removeObserver(currentWeatherObserver)
        // viewModel.forecastData.removeObserver(forecastListObserver)
    }


    @Test
    fun initTest() {
        assert(true)
    }

    /*
    @Test
    fun getCurrentWeather_Success() {
        val data = ResourceState(
            Status.SUCCESS,
            testData.getWeather(),
            null
        )

        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("London")).thenReturn(
            Observable.just(
                data
            )
        )

        viewModel.getCurrentWeatherData("London")
        verify(currentWeatherObserver).onChanged(ResourceState(Status.LOADING, null, null))
        Thread.sleep(2000)
        verify(currentWeatherObserver).onChanged(
            data
        )
    }

    @Test
    fun getCurrentWeather_Error() {
        val data = ResourceState(Status.ERROR, null, BaseError("An error"))

        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("Zag")).thenReturn(
            Observable.just(data)
        )

        viewModel.getCurrentWeatherData("Zag")
        verify(currentWeatherObserver).onChanged(ResourceState(Status.LOADING, null, null))
        Thread.sleep(2000)
        verify(currentWeatherObserver).onChanged(data)
    }

    @Test
    fun getForecastData_Success() {
        val data = ResourceState(Status.SUCCESS, testData.getForecastData(), null)
        val expectedData = ResourceState(Status.SUCCESS, testData.getSortedForecastData(), null)

        Mockito.`when`(weatherRepo.getForecastForCity("Zagreb")).thenReturn(
            Observable.just(data)
        )

        viewModel.getForecastData("Zagreb", 1571220000000)
        verify(forecastListObserver).onChanged(ResourceState(Status.LOADING, null, null))
        Thread.sleep(2000)
        verify(forecastListObserver).onChanged(expectedData)
    }

    @Test
    fun getForecastData_NoItems() {
        val data = ResourceState(Status.SUCCESS, testData.getForecastData(), null)
        val expectedData = ResourceState(Status.SUCCESS, listOf<Forecast>(), null)

        Mockito.`when`(weatherRepo.getForecastForCity("Zagreb")).thenReturn(
            Observable.just(data)
        )

        viewModel.getForecastData("Zagreb", 0)
        verify(forecastListObserver).onChanged(ResourceState(Status.LOADING, null, null))
        Thread.sleep(2000)
        verify(forecastListObserver).onChanged(expectedData)
    }

    @Test
    fun getForecastData_Error() {
        val data = ResourceState(Status.ERROR, null, BaseError("An error"))

        Mockito.`when`(weatherRepo.getForecastForCity("Zagr")).thenReturn(Observable.just(data))

        viewModel.getForecastData("Zagr", 1571220000000)
        verify(forecastListObserver).onChanged(ResourceState(Status.LOADING, null, null))
        Thread.sleep(2000)
        verify(forecastListObserver).onChanged(data)
    }

     */

}