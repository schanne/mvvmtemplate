package com.bornfight.common.mvvm

import androidx.lifecycle.Observer
import com.bornfight.common.mvvm.basemodels.BaseError
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.ResponseCodes
import com.bornfight.common.mvvm.basemodels.Status

/**
 * Created by lleopoldovic on 02/10/2019.
 */

class ResourceStateObserver<T>(
    private val view: BaseView,
    private val onSuccess: (data: T?) -> Unit,
    private val onLoading: (isLoading: Boolean) -> Unit = DEFAULT_LOADING_ACTION,
    private val onError: (baseError: BaseError) -> Unit = DEFAULT_ERROR_ACTION
) : Observer<ResourceState<T>> {

    companion object {
        private val DEFAULT_ERROR_ACTION: (baseError: BaseError) -> Unit = {}
        private val DEFAULT_LOADING_ACTION: (isLoading: Boolean) -> Unit = {}
    }

    override fun onChanged(t: ResourceState<T>?) {
        // Loading state handler:
        onLoadingActionCheck(onLoading, isLoading = t?.status == Status.LOADING)

        // Error/success state handler:
        if (t?.status == Status.ERROR) {
            onErrorActionCheck(onError, baseError = t.error ?: BaseError(errorMessage = BaseError.baseErrorMessage))
            // If a response code needs to be handled specifically, add the new case here
            when (t.error?.errorCode) {
                ResponseCodes.UNAUTHORIZED.code -> view.onLogout()
            }

        } else if (t?.status == Status.SUCCESS) {
            onSuccess(t.data)
        }
    }

    private fun onLoadingActionCheck(
        onLoadingAction: (isLoading: Boolean) -> Unit = DEFAULT_LOADING_ACTION,
        isLoading: Boolean
    ) {
        if (onLoadingAction === DEFAULT_LOADING_ACTION) {
            // A lambda is NOT defined - use the default value
            view.showProgressCircle(isLoading)
        } else {
            // A lambda is defined - no default value used
            onLoading(isLoading)
        }
    }

    private fun onErrorActionCheck(
        onErrorAction: (baseError: BaseError) -> Unit = DEFAULT_ERROR_ACTION,
        baseError: BaseError
    ) {
        if (onErrorAction === DEFAULT_ERROR_ACTION) {
            // A lambda is NOT defined - use the default value
            view.showError(baseError.errorMessage)
        } else {
            // A lambda is defined - no default value used
            onError(baseError)
        }
    }
}