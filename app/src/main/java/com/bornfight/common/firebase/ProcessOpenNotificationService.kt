package com.bornfight.common.firebase

import android.app.IntentService
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import com.bornfight.appname.BuildConfig
import com.bornfight.common.data.retrofit.ApiInterface
import com.crashlytics.android.Crashlytics
import dagger.android.AndroidInjection
import org.parceler.Parcels
import java.util.*
import javax.inject.Inject


/**
 * ProcessOpenNotificationService is triggered when an pending notification is clicked on.
 * It can notify backend if the notification was opened or dismissed, depending on the intent action.
 **/

class ProcessOpenNotificationService @Inject constructor() : IntentService("ProcessOpenNotificationService") {

    @Inject
    lateinit var mApiInterface: ApiInterface

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.d("ProcessOpenNotification", "data")

        if (intent != null) {
            val action = intent.action
            if (ACTION_OPEN == action) {
                val pi = intent.getParcelableExtra<PendingIntent>(PENDING_INTENT)
                val data = Parcels.unwrap<HashMap<String, String>>(
                    intent.getParcelableExtra<Parcelable>(
                        NOTIF_DATA
                    )
                )
                handlePendingIntent(pi, data)
                logNotificationAction(data["id"], API_ACTION_OPENED)
            } else if (ACTION_DISMISS == action) {
                val data = Parcels.unwrap<HashMap<String, String>>(
                    intent.getParcelableExtra<Parcelable>(
                        NOTIF_DATA
                    )
                )
                logNotificationAction(data["id"], API_ACTION_DISMISSED)
            }
        }
    }

    private fun handlePendingIntent(pendingIntent: PendingIntent, data: HashMap<String, String>?) {
        Log.d("ProcessOpenNotification", "data is $data")
        try {
            pendingIntent.send()
        } catch (e: PendingIntent.CanceledException) {
            e.printStackTrace()
            Crashlytics.logException(e)
        }

    }

    private fun logNotificationAction(notifId: String?, action: String) {
        //TODO: log notification action
        /*
        mApiInterface.sendPushAction(
            notifId,
            action
        ).subscribeOn(Schedulers.io())
            .subscribe(new DefaultSubscriber<Void>());
            */
    }

    companion object {

        private const val API_ACTION_OPENED = "opened"
        private const val API_ACTION_DISMISSED = "dismissed"

        private const val ACTION_OPEN = BuildConfig.APPLICATION_ID + ".action.OPEN"
        private const val ACTION_DISMISS = BuildConfig.APPLICATION_ID + ".action.DISMISS"
        private const val PENDING_INTENT = BuildConfig.APPLICATION_ID + ".extra.PI"
        private const val NOTIF_DATA = BuildConfig.APPLICATION_ID + ".extra.NOTIF_DATA"

        /**
         * Builds pending intent which will trigger an API request to backend with [API_ACTION_OPENED] action.
         *
         * @see ProcessOpenNotificationService
         */
        fun buildPendingOpenIntent(context: Context, pendingIntent: PendingIntent, data: Map<String, String>): Intent {
            val intent = Intent(context, ProcessOpenNotificationService::class.java)
            intent.action = ACTION_OPEN
            intent.putExtra(PENDING_INTENT, pendingIntent)
            intent.putExtra(NOTIF_DATA, Parcels.wrap(HashMap(data)))
            return intent
        }

        /**
         * Builds pending intent which will trigger an API request to backend with [API_ACTION_DISMISSED] action.
         *
         * @see ProcessOpenNotificationService
         */
        fun buildDismissIntent(context: Context, data: Map<String, String>): Intent {
            val intent = Intent(context, ProcessOpenNotificationService::class.java)
            intent.action = ACTION_DISMISS
            intent.putExtra(NOTIF_DATA, Parcels.wrap(HashMap(data)))
            return intent
        }
    }
}
