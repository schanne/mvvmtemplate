package com.bornfight.common.dagger.modules

import com.bornfight.common.firebase.MyFirebaseMessagingService
import com.bornfight.common.firebase.ProcessOpenNotificationService
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by tsmrecki on 15/01/2019.
 */
@Module
abstract class BindingModuleServices {

    // Bind every new service here, following the examples:
    @ContributesAndroidInjector
    abstract fun contributeFirebaseMessagingService(): MyFirebaseMessagingService

    @ContributesAndroidInjector
    abstract fun contributeProcessOpenNotificationService(): ProcessOpenNotificationService
}