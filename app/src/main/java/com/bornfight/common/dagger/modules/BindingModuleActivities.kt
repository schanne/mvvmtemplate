package com.bornfight.common.dagger.modules

import com.bornfight.appname.main.MainActivity
import com.bornfight.common.mvvm.BaseActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by lleopoldovic on 23/09/2019.
 */
@Module
abstract class BindingModuleActivities {

    // Bind every new activity here, following the BaseActivity example:
    @ContributesAndroidInjector
    abstract fun contributeBaseActivity(): BaseActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

}