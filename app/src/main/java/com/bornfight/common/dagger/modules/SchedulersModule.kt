package com.bornfight.common.dagger.modules

import com.bornfight.common.scheduler.AndroidSchedulerProvider
import com.bornfight.common.scheduler.SchedulerProvider
import dagger.Binds
import dagger.Module

/**
 * Created by lleopoldovic on 23/09/2019.
 */

@Module
abstract class SchedulersModule {

    @Binds
    abstract fun bindSchedulerProvider(androidSchedulerProvider: AndroidSchedulerProvider): SchedulerProvider

}
