package com.bornfight.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.News
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.demo.weather.model.WeatherResponse
import com.bornfight.demo.weather.model.dao.NewsDao
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionDao
import com.bornfight.demo.weather.repository.weather.WeatherDao

/**
 * Created by lleopoldovic on 13/09/2019.
 */

@Database(
    entities = [SearchSuggestion::class, WeatherResponse::class, Forecast::class, News::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    // Provide each dao here, following the SearchSuggestionDao example.
    // ** Each entity used by any Dao, must be included within entities field of @Database annotation. **

    abstract fun searchSuggestionDao(): SearchSuggestionDao

    abstract fun weatherDao(): WeatherDao

    abstract fun newsDao(): NewsDao

}