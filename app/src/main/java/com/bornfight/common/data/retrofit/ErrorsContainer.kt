package com.bornfight.common.data.retrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by tomislav on 09/01/2017.
 */

class ErrorsContainer<T> {

    @SerializedName("errors")
    internal var errors: T? = null

}
