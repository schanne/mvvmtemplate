package com.bornfight.common.data.retrofit

import android.util.Log

import io.reactivex.observers.DefaultObserver


/**
 * Created by tomislav on 08/03/2017.
 */

class EmptyObserver<T> : DefaultObserver<T>() {

    override fun onComplete() {

    }

    override fun onError(e: Throwable) {
        Log.d(TAG, "onError: " + e.localizedMessage)
    }

    override fun onNext(t: T) {
        Log.d(TAG, "Success")
    }

    companion object {

        private const val TAG = "EmptyObserver"
    }
}
