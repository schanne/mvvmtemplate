package com.bornfight.common.session

/**
 * Created by tomislav on 16/12/2016.
 */
interface Session {

    val isLoggedIn: Boolean

    var token: String

    var userId: Long

    fun setLoggedIn(loggedIn: Boolean, userId: Long)

    fun logout()

    fun checkPermissionAsked(permission: String): Boolean

    fun setPermissionAsked(permission: String)

    fun shouldSkipOnboardingStep(stepName: String): Boolean

    fun setSkipOnboardingStep(stepName: String, skip: Boolean)

    fun isTipsShown(tipSet: String): Boolean

    fun setTipsShown(tipSet: String, shown: Boolean)
}
