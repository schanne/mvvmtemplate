package com.bornfight.appname.main

import android.os.Bundle
import com.bornfight.appname.R
import com.bornfight.common.firebase.ProcessOpenNotificationService
import com.bornfight.common.mvvm.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startService(
            ProcessOpenNotificationService.buildDismissIntent(
                this,
                HashMap<String, String>().apply { put("id", "10") })
        )
    }
}