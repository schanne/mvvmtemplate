package com.bornfight.demo.weather.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

@Entity(primaryKeys = ["date", "name"])
@TypeConverters(WeatherConverter::class)
data class Forecast(
    @Embedded
    @SerializedName("main")
    val main: Main,
    @SerializedName("weather")
    val weather: List<Weather>,
    @SerializedName("dt_txt")
    val date: String,
    val name: String = ""
) {

    constructor() : this(Main(0.0, 0.0, 0.0), emptyList<Weather>(), "", "")

    fun getImageUrl(): String {
        return "https://openweathermap.org/img/w/${weather[0].icon}.png"
    }

    fun getTimeString(): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val format = SimpleDateFormat("HH:mm", Locale.US)
        return format.format(dateFormat.parse(date))
    }

    fun getDateString(): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val format = SimpleDateFormat("dd.MM.", Locale.US)
        return format.format(dateFormat.parse(date))
    }

    fun isSelectedDate(date: Long): Boolean {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return this.date.contains(format.format(date))
    }

}