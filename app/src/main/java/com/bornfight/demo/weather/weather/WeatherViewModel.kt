package com.bornfight.demo.weather.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bornfight.common.mvvm.BaseViewModel
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import com.bornfight.demo.weather.repository.weather.WeatherRepo
import javax.inject.Inject

class WeatherViewModel @Inject constructor(
    private val schedulers: SchedulerProvider,
    private val repo: WeatherRepo
) : BaseViewModel(schedulers) {

    private val _weatherData = MutableLiveData<ResourceState<WeatherResponse>>()
    val weatherData: LiveData<ResourceState<WeatherResponse>> = _weatherData
    private val _forecastData = MutableLiveData<ResourceState<List<Forecast>>>()
    val forecastData: LiveData<ResourceState<List<Forecast>>> = _forecastData

    fun getCurrentWeatherData(city: String) {
        observableCall(_weatherData, {
            repo.getCurrentWeatherForCity(city)
        })
    }

    fun getForecastData(city: String, date: Long) {
        observableCall(_forecastData, {
            repo.getForecastForCity(city).filterData {
                it.isSelectedDate(date)
            }
        })
    }

}