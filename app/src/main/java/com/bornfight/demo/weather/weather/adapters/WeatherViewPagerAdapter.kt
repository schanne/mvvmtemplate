package com.bornfight.demo.weather.weather.adapters

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bornfight.demo.weather.weather.WeatherFragment

class WeatherViewPagerAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    private val cityName: String
) : FragmentStateAdapter(fm, lifecycle) {

    private val itemList: MutableList<Long> = mutableListOf()

    fun setItemList(itemList: List<Long>) {
        this.itemList.clear()
        this.itemList.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = itemList.size

    override fun createFragment(position: Int) =
        WeatherFragment.newInstance(cityName, itemList[position], position == 0)


}