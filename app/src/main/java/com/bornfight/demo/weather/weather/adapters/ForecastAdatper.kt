package com.bornfight.demo.weather.weather.adapters

import android.view.View
import com.bornfight.appname.R
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.utils.adapters.GenericAdapter
import com.bornfight.utils.kelvinToCelsiusString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_forecast.view.*

class ForecastAdatper : GenericAdapter<Forecast>() {

    override fun getLayoutId(viewType: Int) = R.layout.item_forecast

    override fun getViewHolder(view: View, viewType: Int): GenericViewHolder<Forecast> {
        return ForecastViewHolder(view)
    }

    inner class ForecastViewHolder(itemView: View) :
        GenericAdapter.GenericViewHolder<Forecast>(itemView) {
        override fun bind(data: Forecast) {
            Glide.with(itemView.weatherIconIV).load(data.getImageUrl()).into(itemView.weatherIconIV)
            itemView.dateTV.text = data.getTimeString()
            itemView.temperatureTV.text = data.main.temp.kelvinToCelsiusString()
            itemView.messageTV.text = data.weather[0].description
        }
    }
}