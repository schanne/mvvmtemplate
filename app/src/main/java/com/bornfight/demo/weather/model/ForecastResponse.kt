package com.bornfight.demo.weather.model

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    @SerializedName("list")
    val forecastList: List<Forecast>
)