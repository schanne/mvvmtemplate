package com.bornfight.demo.weather.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by lleopoldovic on 13/09/2019.
 */

@Entity
data class SearchSuggestion(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "suggestion")
    val suggestion: String = ""
) {
}
