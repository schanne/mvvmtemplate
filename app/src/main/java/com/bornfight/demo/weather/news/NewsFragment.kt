package com.bornfight.demo.weather.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bornfight.appname.R
import com.bornfight.common.mvvm.BaseFragment
import com.bornfight.common.mvvm.ResourceStateObserver
import com.bornfight.demo.weather.news.adapters.NewsListAdapter
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.fragment_news.*
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class NewsFragment : BaseFragment() {

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         */
        @JvmStatic
        fun newInstance() = NewsFragment()
    }

    private val newsListAdapter: NewsListAdapter = NewsListAdapter()

    private val viewModel: NewsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        bindUI()
    }

    private fun setupUI() {
        news_rv.layoutManager = LinearLayoutManager(news_rv.context)
        news_rv.adapter = newsListAdapter

        RxTextView.textChanges(search_et)
            .debounce(400, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<CharSequence>() {
                override fun onNext(t: CharSequence?) { viewModel.search(t.toString()) }
                override fun onCompleted() { }
                override fun onError(e: Throwable?) { }
            })
    }

    private fun bindUI() {
        viewModel.newsList.observe(viewLifecycleOwner, ResourceStateObserver(this,
            onSuccess = { it?.let { data -> newsListAdapter.submitList(data) } }
        ))
    }
}
