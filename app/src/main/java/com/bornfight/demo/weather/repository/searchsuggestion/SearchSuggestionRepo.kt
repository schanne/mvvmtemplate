package com.bornfight.demo.weather.repository.searchsuggestion

import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.demo.weather.model.SearchSuggestion
import io.reactivex.Observable

interface SearchSuggestionRepo {

    fun getSearchSuggestions(): Observable<ResourceState<List<SearchSuggestion>>>

    fun saveSearchSuggestion(searchSuggestion: SearchSuggestion)

}