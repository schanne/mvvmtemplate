package com.bornfight.demo.weather.searchsuggestions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bornfight.appname.R
import com.bornfight.common.mvvm.BaseFragment
import com.bornfight.common.mvvm.ResourceStateObserver
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.demo.weather.searchsuggestions.adapters.SearchSuggestionsAdapter
import kotlinx.android.synthetic.main.fragment_search_suggestions.*

/**
 * Created by lleopoldovic on 13/09/2019.
 */

class SearchSuggestionsFragment : BaseFragment() {

    // Private vars
    private val viewModel: SearchSuggestionsViewModel by viewModels { viewModelFactory }
    private var searchSuggestionAdapter =
        SearchSuggestionsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_suggestions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchSuggestionsRV.layoutManager = LinearLayoutManager(view.context)
        searchSuggestionsRV.adapter = searchSuggestionAdapter
        searchSuggestionsRV.isMotionEventSplittingEnabled = false

        addSearchSuggestionBtn.setOnClickListener {
            if (!searchET.text.toString().isBlank()) {
                val text = searchET.text.toString()

                viewModel.saveSearchSuggestion(SearchSuggestion(suggestion = searchET.text.toString()))
                searchET.text.clear()
                hideKeyboard(it)

                findNavController().navigate(
                    SearchSuggestionsFragmentDirections.actionSearchSuggestionsToWeatherViewPager2(
                        text
                    )
                )
            }
        }

        bindUI()
    }

    // UI updates here:
    private fun bindUI() {
        viewModel.searchSuggestions.observe(
            viewLifecycleOwner, ResourceStateObserver(this,
                {
                    it?.let { searchSuggestions -> updateSearchSuggestionsRV(searchSuggestions) }
                })
        )
    }

    private fun updateSearchSuggestionsRV(searchSuggestions: List<SearchSuggestion>) {
        searchSuggestionAdapter.setItems(searchSuggestions)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         */
        @JvmStatic
        fun newInstance() = SearchSuggestionsFragment()
    }
}